<?php
/**
 * A tree that prints its stuff out
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Utilitree;

class PrintingTree extends SortedTree
{
    /**
     * Print in order
     */
    public function printInOrder()
    {
        if (empty($this->rootNode)) return;

        $this->printInOrderRecursive($this->rootNode);
    }

    /**
     * Print in order recursive
     *
     * @param $node
     */
    private function printInOrderRecursive(&$node)
    {
        if (!empty($node->leftChild))
        {
            $this->printInOrderRecursive($node->leftChild);
        }

        echo $node->value . ' ';

        if (!empty($node->rightChild))
        {
            $this->printInOrderRecursive($node->rightChild);
        }
    }

    /**
     * Print in breadth first order.
     *
     * The technique here is to store a queue, as you go through each node you enqueue it's children. Here
     * we are also storing the depth alongside the node so we can print a pipe.
     *
     * This is based around the example I saw in the livestream.
     */
    public function printBreadthFirst()
    {
        if (empty($this->rootNode))
        {
            return;
        }

        $items = [[$this->rootNode,0]];
        $currentDepth = 0;

        while (!empty($items))
        {
            list($print, $depth) = array_shift($items);

            if ($depth > $currentDepth) echo '| ';
            $currentDepth = $depth;

            echo $print->value . ' ';

            if (!empty($print->leftChild))
            {
                $items[] = [$print->leftChild, $depth + 1];
            }

            if (!empty($print->rightChild))
            {
                $items[] = [$print->rightChild, $depth + 1];
            }
        }
    }

}