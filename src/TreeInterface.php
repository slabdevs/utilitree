<?php
/**
 * Tree Interface
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Utilitree;

interface TreeInterface
{
    /**
     * Add an element to the tree somehow and then return the tree
     *
     * @param $value
     * @return TreeInterface
     */
    public function addElement($value);
}