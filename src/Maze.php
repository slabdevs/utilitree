<?php
/**
 * A maze tree for testing A*
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Utilitree;

class Maze
{
    /**
     * @var Nodes\MazeNode
     */
    private $root;

    /**
     * @var Nodes\MazeNode
     */
    private $end;

    /**
     * @var Nodes\MazeNode[]
     */
    private $nodeMap = [];

    /**
     * Generate a Maze from a String
     * ex.
     *
     * ###########
     * #S#  #    #
     * # ## # ####
     * # #    # E#
     * #   ##   ##
     * ###########
     * @param $string
     */
    public function generateMazeFromString($string)
    {
        $characters = strlen($string);

        $x=$y=0;
        for ($i=0; $i<$characters; ++$i)
        {
            $character = $string[$i];
            if ($character == "\n")
            {
                $y++;
                $x = 0;
                continue;
            }

            if ($character == "\t") continue;
            if ($character == "\r") continue;

            $this->nodeMap[$y][$x] = new Nodes\MazeNode();
            $mazeNode =& $this->nodeMap[$y][$x];

            if ($character == '#')
            {
                $mazeNode->value = -1;
            }

            if ($character === 'S')
            {
                $this->root =& $this->nodeMap[$y][$x];
            }

            if ($character === 'E')
            {
                $this->end =& $this->nodeMap[$y][$x];
            }

            if (!empty($this->nodeMap[$y][$x-1]))
            {
                $mazeNode->west =& $this->nodeMap[$y][$x-1];
                $this->nodeMap[$y][$x-1]->east =& $mazeNode;
            }

            if (!empty($this->nodeMap[$y-1][$x]))
            {
                $mazeNode->north =& $this->nodeMap[$y-1][$x];
                $this->nodeMap[$y-1][$x]->south =& $mazeNode;
            }

            ++$x;
        }

        if (empty($this->root))
        {
            throw new \Exception("The maze does not have a start!");
        }

        if (empty($this->end))
        {
            throw new \Exception("The maze does not have an end!");
        }

        if ($this->root === $this->end)
        {
            throw new \Exception("The start and end are the same!");
        }

        $this->assignMazeValues($this->end, 0);
    }

    /**
     * Spider out from the node and increase the value by 1
     *
     * @param Nodes\MazeNode $node
     * @param $value
     */
    private function assignMazeValues(Nodes\MazeNode &$node, $value)
    {
        $node->value = $value;

        $nextValue = $value + 1;

        if (!empty($node->west) && ($node->west->value == 0 || $node->west->value > $nextValue))
        {
            $this->assignMazeValues($node->west, $nextValue);
        }

        if (!empty($node->north) && ($node->north->value == 0 || $node->north->value > $nextValue))
        {
            $this->assignMazeValues($node->north, $nextValue);
        }

        if (!empty($node->south) && ($node->south->value == 0 || $node->south->value > $nextValue))
        {
            $this->assignMazeValues($node->south, $nextValue);
        }

        if (!empty($node->east) && ($node->east->value == 0 || $node->east->value > $nextValue))
        {
            $this->assignMazeValues($node->east, $nextValue);
        }
    }

    /**
     * Traverse the maze by continuously finding the lowest next adjacent value
     *
     * @return bool
     */
    public function traverseMaze()
    {
        $done = false;
        $node =& $this->root;
        $steps = 0;

        while (!$done)
        {
            if (empty($node))
            {
                return false;
            }

            if ($node === $this->end)
            {
                return $steps;
            }

            $lowest = null;

            if (!empty($node->east) && $node->east->value > 0)
            {
                if (empty($lowest) || $node->east->value < $lowest->value)
                {
                    $lowest =& $node->east;
                }
            }
            if (!empty($node->west) && $node->west->value > 0)
            {
                if (empty($lowest) || $node->west->value < $lowest->value)
                {
                    $lowest =& $node->west;
                }
            }
            if (!empty($node->north) && $node->north->value > 0)
            {
                if (empty($lowest) || $node->north->value < $lowest->value)
                {
                    $lowest =& $node->north;
                }
            }
            if (!empty($node->south) && $node->south->value > 0)
            {
                if (empty($lowest) || $node->south->value < $lowest->value)
                {
                    $lowest =& $node->south;
                }
            }

            $node = $lowest;
            $steps++;
        }

        return false;
    }

    /**
     * Print maze
     */
    public function printMaze()
    {
        foreach ($this->nodeMap as $yIndex=>$row)
        {
            foreach ($row as $field)
            {
                if ($field->value == '-1')
                {
                    echo '#';
                }
                else if ($field == $this->root)
                {
                    echo 'S';
                }
                else if ($field == $this->end)
                {
                    echo 'E';
                }
                else
                {
                    if ($field->value > 9)
                        echo '.';
                    else
                        echo $field->value;
                }
            }

            if (!empty($this->nodeMap[$yIndex + 1]))
                echo "\n";
        }
    }
}