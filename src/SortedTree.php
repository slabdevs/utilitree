<?php
/**
 * Sorted Tree Class with customizable Comparator Function
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Utilitree;

class SortedTree implements TreeInterface
{
    /**
     * @var Nodes\TwoChildren
     */
    protected $rootNode;

    /**
     * @var mixed
     */
    private $comparatorFunction;

    /**
     * Set the default comparator function
     */
    public function __construct()
    {
        $this->comparatorFunction = function(&$a, &$b){
            if ($a < $b) return -1;
            if ($a == $b) return 0;
            return 1;
        };
    }

    /**
     * Set comparator function
     *
     * @param $comparatorFunction
     * @return $this
     */
    public function setComparatorFunction($comparatorFunction)
    {
        $this->comparatorFunction = $comparatorFunction;

        return $this;
    }

    /**
     * Add an element to the tree sorted
     *
     * @param $value
     * @return mixed
     */
    public function addElement($value)
    {
        if (empty($this->rootNode))
        {
            $this->rootNode = new Nodes\TwoChildren($value);

            return $this;
        }

        $this->addElementRecursiveSorted($value, $this->rootNode);
    }

    /**
     * Add an element to the tree recursively but sorting it against comparator function
     *
     * @param $value
     * @param $node
     */
    private function addElementRecursiveSorted($value, &$node)
    {
        $comparatorFunction = $this->comparatorFunction;
        $comparison = $comparatorFunction($value, $node->value);

        if ($comparison < 0)
        {
            if (empty($node->leftChild))
            {
                $node->leftChild = new Nodes\TwoChildren($value);
                return;
            }
            else
            {
                $this->addElementRecursiveSorted($value, $node->leftChild);
            }
        }
        else
        {
            if (empty($node->rightChild))
            {
                $node->rightChild = new Nodes\TwoChildren($value);
                return;
            }
            else
            {
                $this->addElementRecursiveSorted($value, $node->rightChild);
            }
        }
    }

    /**
     * Search the tree for a specific value and return true if it exists
     *
     * @param $value
     */
    public function search($value)
    {
        if (empty($this->rootNode))
        {
            return false;
        }

        return $this->searchRecursive($value, $this->rootNode);
    }

    /**
     * Search recursively down the tree for a value
     *
     * @param $value
     * @param $node
     * @return bool
     */
    private function searchRecursive($value, &$node)
    {
        $compareFunction = $this->comparatorFunction;
        $comparison = $compareFunction($value, $node->value);

        if ($comparison == 0)
        {
            return true;
        }

        if ($comparison < 0)
        {
            if (empty($node->leftChild))
            {
                return false;
            }

            return $this->searchRecursive($value, $node->leftChild);
        }

        if (empty($node->rightChild))
        {
            return false;
        }

        return $this->searchRecursive($value, $node->rightChild);
    }

    /**
     * Get the minimum items from the tree. Do an in-order traversal of the tree and append items to the
     * end of the match list. Then return the array slice.
     *
     * Could be optimized by sending numberOfItems into inOrderTraverse and cutting off traversal when the
     * count has reached the number of items.
     *
     * @param int $numberOfItems
     */
    public function getMinimum($numberOfItems = 1)
    {
        if (empty($this->rootNode))
        {
            return [];
        }

        $matches = [];
        for ($i=0; $i<$numberOfItems; ++$i)
        {
            $this->inOrderTraverse($this->rootNode, $matches);
        }

        return array_slice($matches, 0, $numberOfItems);
    }

    /**
     * In order traverse the tree
     *
     * @param $node
     * @param $matches
     */
    private function inOrderTraverse(&$node, &$matches)
    {
        if (!empty($node->leftChild))
        {
            $this->inOrderTraverse($node->leftChild, $matches);
        }

        $matches[] = $node->value;

        if (!empty($node->rightChild))
        {
            $this->inOrderTraverse($node->rightChild, $matches);
        }
    }

    /**
     * Get the maximum items from the tree. Do an in-order traversal and push items to the beginning of the
     * array rather than append them to the end.
     *
     * Could be optimized by sending numberOfItems into inOrderReverseTraverse and cutting off traversal when the
     * count has reached the number of items.
     *
     * @param int $numberOfItems
     */
    public function getMaximum($numberOfItems = 1)
    {
        if (empty($this->rootNode))
        {
            return [];
        }

        $matches = [];
        for ($i=0; $i<$numberOfItems; ++$i)
        {
            $this->inOrderReverseTraverse($this->rootNode, $matches);
        }

        return array_slice($matches, 0, $numberOfItems);
    }

    /**
     * In-order but push stuff onto the list instead of append to the end
     *
     * @param $node
     * @param $matches
     */
    private function inOrderReverseTraverse(&$node, &$matches)
    {
        if (!empty($node->leftChild))
        {
            $this->inOrderReverseTraverse($node->leftChild, $matches);
        }

        array_unshift($matches, $node->value);

        if (!empty($node->rightChild))
        {
            $this->inOrderReverseTraverse($node->rightChild, $matches);
        }
    }
}