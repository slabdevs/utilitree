<?php
/**
 * Tree Node with Two Children
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Utilitree\Nodes;

class TwoChildren
{
    /**
     * @var mixed
     */
    public $value;

    /**
     * @var TwoChildren
     */
    public $leftChild;

    /**
     * @var TwoChildren
     */
    public $rightChild;

    /**
     * Shortcut to build a Node2 with a value
     *
     * @param mixed $value
     */
    public function __construct($value = null)
    {
        $this->value = $value;
    }
}