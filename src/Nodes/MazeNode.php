<?php
/**
 * Maze Node
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Utilitree\Nodes;

class MazeNode
{
    /**
     * @var MazeNode
     */
    public $north;

    /**
     * @var MazeNode
     */
    public $west;

    /**
     * @var MazeNode
     */
    public $south;

    /**
     * @var MazeNode
     */
    public $east;

    /**
     * @var integer
     */
    public $value;
}