<?php
/**
 * Test for the Maze
 *
 * @package Slacademic
 * @subpackage Tests
 * @author Eric
 */
namespace Slacademic\Tests\Utilitree;

class MazeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Slacademic\Utilitree\Maze
     */
    private $maze;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->maze = new \Slacademic\Utilitree\Maze();
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProviderForMazeGeneration()
    {
        return [
            [file_get_contents('tests/mazes/first.txt'), file_get_contents('tests/mazes/first-parsed.txt')],
            [file_get_contents('tests/mazes/second.txt'), file_get_contents('tests/mazes/second-parsed.txt')]
        ];
    }

    /**
     * Test maze print
     *
     * @dataProvider dataProviderForMazeGeneration
     */
    public function testMazePrint($testMaze, $testMazeOutput)
    {
        $this->maze->generateMazeFromString($testMaze);

        $this->expectOutputString(trim($testMazeOutput));

        $this->maze->printMaze();
    }
}
