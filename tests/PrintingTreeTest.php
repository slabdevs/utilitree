<?php
/**
 * Test case for printing tree
 *
 * @package Slacademic
 * @subpackage Tests
 * @author Eric
 */
namespace Slacademic\Tests\Utilitree;

class PrintingTreeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test print in order
     *
     * @param $inputs
     * @param $output
     * @dataProvider dataProviderInOrderPrinting
     */
    public function testInOrderPrinting($inputs, $output)
    {
        $tree = new \Slacademic\Utilitree\PrintingTree();
        foreach ($inputs as $input)
        {
            $tree->addElement($input);
        }

        $this->expectOutputString($output);
        $tree->printInOrder();
    }

    /**
     * Data provider for in order printing
     *
     * @return array
     */
    public function dataProviderInOrderPrinting()
    {
        return [
            [[5, 6, 1, 3, 6, 2, 6, 8, 0], '0 1 2 3 5 6 6 6 8 '],
            [[7, 13, 244, 96, 123, 23], '7 13 23 96 123 244 '],
            [[498198, 7384738, 123, 1231], '123 1231 498198 7384738 '],
            [[], ''],
            [[431], '431 ']
        ];
    }


    /**
     * Test for breadth first printing
     *
     * @param $inputs
     * @param $output
     * @dataProvider dataProviderBreadthFirstPrinting
     */
    public function testBreadthFirstPrinting($inputs, $output)
    {
        $tree = new \Slacademic\Utilitree\PrintingTree();
        foreach ($inputs as $input)
        {
            $tree->addElement($input);
        }

        $this->expectOutputString($output);
        $tree->printBreadthFirst();
    }

    /**
     * Data provider for breadth first printing
     *
     * @return array
     */
    public function dataProviderBreadthFirstPrinting()
    {
//                5
//              1   6
//             0 3    7
//              2       9
//                    8

//                7
//                   13
//                      244
//                     96
//                    23  123

//        498198
//       123    7384738
//          1231

        return [
            [[5, 6, 1, 3, 7, 2, 9, 8, 0], '5 | 1 6 | 0 3 7 | 2 9 | 8 '],
            [[7, 13, 244, 96, 123, 23], '7 | 13 | 244 | 96 | 23 123 '],
            [[498198, 7384738, 123, 1231], '498198 | 123 7384738 | 1231 '],
            [[], ''],
            [[431], '431 ']
        ];
    }

}