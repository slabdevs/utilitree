<?php
/**
 * Test for Sorted Tree
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Tests\Utilitree;

class SortedTreeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test the Search Feature of the Sorted Tree against valid and invalid values
     *
     * @covers \Slacademic\Utilitree\SortedTree::addElement
     * @covers \Slacademic\Utilitree\SortedTree::addElementRecursiveSorted
     * @covers \Slacademic\Utilitree\SortedTree::search
     *
     * @param $numbers
     * @param $invalidNumbers
     * @dataProvider dataProviderForTestSearch
     */
    public function testSearch($numbers, $invalidNumbers)
    {
        $tree = new \Slacademic\Utilitree\SortedTree();

        foreach ($numbers as $number)
        {
            $tree->addElement($number);
        }

        foreach ($numbers as $number)
        {
            $this->assertTrue($tree->search($number));
        }

        foreach ($invalidNumbers as $number)
        {
            $this->assertFalse($tree->search($number));
        }
    }

    /**
     * @return array
     */
    public function dataProviderForTestSearch()
    {
        return [
            [[4, 5, 9, 10, 51, 13, 53, 23, 14, 54, 123, 54, 123, 5], [83, 55, 66, 77, 454, 124, 22]],
            [[4, 5, 1, 237, 41, 3, 2, 6, 8, 5], [7, 33, 47, 88, 92, -1, 39]],
            [[989, 5, 47, 161616, 54125, 2345, 542455, 7662, 452345, 762434], [45, 414, 632, 651, 885, 423, 98]]
        ];
    }

    /**
     * Test the custom comparator function for the tree to sort complex objects
     *
     * @covers \Slacademic\Utilitree\SortedTree::setComparatorFunction
     * @covers \Slacademic\Utilitree\SortedTree::search
     * @covers \Slacademic\Utilitree\SortedTree::addElement
     *
     * @dataProvider dataProviderForComparatorFunction
     *
     * @param $pointList
     */
    public function testComparatorFunction($pointList)
    {
        $tree = new \Slacademic\Utilitree\SortedTree();

        $tree->setComparatorFunction(function(&$a, &$b){
            /**
             * @var Mocks\Point $a
             * @var Mocks\Point $b
             */
            $aDistance = $a->getDistanceFromOrigin();
            $bDistance = $b->getDistanceFromOrigin();

            if ($aDistance < $bDistance)
            {
                return -1;
            }
            else if ($aDistance == $bDistance)
            {
                return 0;
            }

            return 1;
        });

        foreach ($pointList as $point)
        {
            $tree->addElement($point);
        }

        //Test searching complex
        foreach ($pointList as $point)
        {
            $this->assertTrue($tree->search($point));
        }
    }

    /**
     * Provide data for test of the comparator function
     *
     * @return array
     */
    public function dataProviderForComparatorFunction()
    {
        return [
            [[new Mocks\Point(1,6), new Mocks\Point(7,3), new Mocks\Point(4,2), new Mocks\Point(13, 4)]],
            [[new Mocks\Point(6,3), new Mocks\Point(2,7), new Mocks\Point(13,55), new Mocks\Point(22, 3)]],
            [[new Mocks\Point(13,55), new Mocks\Point(22,66), new Mocks\Point(12,44), new Mocks\Point(61, 43)]]
        ];
    }

    /**
     * Test get minimum capability of the tree
     *
     * @covers \Slacademic\Utilitree\SortedTree::getMinimum
     *
     * @dataProvider dataProviderForTestGetMinimum
     *
     * @param $numbers
     * @param $numberOfItems
     * @param $correct
     */
    public function testGetMinimum($numbers, $numberOfItems, $correct)
    {
        $tree = new \Slacademic\Utilitree\SortedTree();

        foreach ($numbers as $number)
        {
            $tree->addElement($number);
        }

        $matches = $tree->getMinimum($numberOfItems);

        $this->assertCount($numberOfItems, $matches);
        $this->assertEquals($matches, $correct);
    }

    /**
     * @return array
     */
    public function dataProviderForTestGetMinimum()
    {
        return [
            [[123, 5, 9, 10, 51, 13, 53, 23, 14, 54, 4, 54, 123, 5], 3, [4, 5, 5]],
            [[4, 5, 1, 237, 41, 3, 2, 6, 8, 5], 4, [1, 2, 3, 4]],
            [[989, 5, 47, 161616, 54125, 2345, 542455, 7662, 452345, 762434], 1, [5]],
            [[6, 7, 4, 2, 3, 1, 5, 10, 9, 8], 10, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]
        ];
    }

    /**
     * Test get minimum capability of the tree
     *
     * @covers \Slacademic\Utilitree\SortedTree::getMaximum
     *
     * @dataProvider dataProviderForTestGetMaximum
     *
     * @param $numbers
     * @param $numberOfItems
     * @param $correct
     */
    public function testGetMaximum($numbers, $numberOfItems, $correct)
    {
        $tree = new \Slacademic\Utilitree\SortedTree();

        foreach ($numbers as $number)
        {
            $tree->addElement($number);
        }

        $matches = $tree->getMaximum($numberOfItems);

        //die('<pre>'.print_r($matches,1));

        $this->assertCount($numberOfItems, $matches);
        $this->assertEquals($matches, $correct);
    }

    /**
     * @return array
     */
    public function dataProviderForTestGetMaximum()
    {
        return [
            [[123, 5, 9, 10, 51, 13, 53, 23, 14, 54, 4, 54, 127, 5], 3, [127, 123, 54]],
            [[4, 5, 1, 237, 41, 3, 2, 6, 8, 5], 4, [237, 41, 8, 6]],
            [[989, 5, 47, 161616, 54125, 2345, 542455, 7662, 452345, 762434], 1, [762434]],
            [[6, 7, 4, 2, 3, 1, 5, 10, 9, 8], 10, [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]]
        ];
    }

    /**
     * Test the custom comparator function for the tree to sort complex objects
     *
     * @covers \Slacademic\Utilitree\SortedTree::setComparatorFunction
     * @covers \Slacademic\Utilitree\SortedTree::getMinimum
     * @covers \Slacademic\Utilitree\SortedTree::getMaximum
     * @covers \Slacademic\Utilitree\SortedTree::addElement
     *
     * @dataProvider dataProviderForComplexMinimumAndMaximum
     *
     * @param $pointList
     * @param Mocks\Point $minimum
     * @param Mocks\Point $maximum
     */
    public function testGetMinimumMaximumOnComplexObject($pointList, Mocks\Point $minimum, Mocks\Point $maximum)
    {
        $tree = new \Slacademic\Utilitree\SortedTree();

        $tree->setComparatorFunction(function(&$a, &$b){
            /**
             * @var Mocks\Point $a
             * @var Mocks\Point $b
             */
            $aDistance = $a->getDistanceFromOrigin();
            $bDistance = $b->getDistanceFromOrigin();

            if ($aDistance < $bDistance)
            {
                return -1;
            }
            else if ($aDistance == $bDistance)
            {
                return 0;
            }

            return 1;
        });

        foreach ($pointList as $point)
        {
            $tree->addElement($point);
        }

        $minimumPoint = $tree->getMinimum(1);
        $this->assertEquals($minimum->getDistanceFromOrigin(), $minimumPoint[0]->getDistanceFromOrigin());
        $this->assertEquals($minimum, $minimumPoint[0]);

        $maximumPoint = $tree->getMaximum(1);
        $this->assertEquals($maximum->getDistanceFromOrigin(), $maximumPoint[0]->getDistanceFromOrigin());
        $this->assertEquals($maximum, $maximumPoint[0]);
    }

    /**
     * Provide data for test of the comparator function
     *
     * @return array
     */
    public function dataProviderForComplexMinimumAndMaximum()
    {
        return [
            [[new Mocks\Point(1,6), new Mocks\Point(7,3), new Mocks\Point(4,2), new Mocks\Point(13, 4)], new Mocks\Point(4,2), new Mocks\Point(13, 4)],
            [[new Mocks\Point(6,3), new Mocks\Point(2,7), new Mocks\Point(13,55), new Mocks\Point(22, 3)], new Mocks\Point(6, 3), new Mocks\Point(13, 55)],
            [[new Mocks\Point(13,55), new Mocks\Point(22,66), new Mocks\Point(12,44), new Mocks\Point(61, 43)], new Mocks\Point(12, 44), new Mocks\Point(61, 43)]
        ];
    }
}