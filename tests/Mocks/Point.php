<?php
/**
 * Mock point class for testing the comparator function with a complex data type
 *
 * @package Slacademic
 * @subpackage Utilitree
 * @author Eric
 */
namespace Slacademic\Tests\Utilitree\Mocks;

class Point
{
    /**
     * @var float
     */
    public $x;

    /**
     * @var float
     */
    public $y;

    /**
     * @var float
     */
    private $distance = null;

    /**
     * Point constructor.
     *
     * @param $x
     * @param $y
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Get distance from origin but leave off the square root call because its slow
     * Distance Formula: sqrt((x2 - x1)^2 + (y2 - y1)^2)
     *
     * @return float
     */
    public function getDistanceFromOrigin()
    {
        if ($this->distance != null)
        {
            return $this->distance;
        }

        //Further optimization we're getting the distance from origin so we don't need to subtract zero
        $this->distance = ($this->x * $this->x) + ($this->y * $this->y);

        return $this->distance;
    }
}