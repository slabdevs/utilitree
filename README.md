# Utilitree Library

Fun tree object project that is being built simply for brain-refreshing and academic value.

You probably shouldn't use this code for anything, I can't guarantee it's right or fast. But if you want to, go ahead.
Attribution is always nice but unnecessary.

## Tests

All development on this was tested with phpunit.

> phpunit --bootstrap vendor/autoload.php tests

## Feedback

Feedback is always appreciated. However for something like this, it was something I built while I was sick with the flu.
I probably wouldn't go back and update it.
